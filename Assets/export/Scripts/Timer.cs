﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    // Start is called before the first frame update
    public Text timer;
    public Canvas end;
    public float time = 60;
    public bool timerIsRunning = false;
    int scoreP2;
    bool over;
    void Start()
    {
        timerIsRunning = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (timerIsRunning)
        {
            if (time > 0)
            {
                time -= Time.deltaTime;
            }
            else
            {
                Debug.Log("Time has run out!");
                time = 0;
                timerIsRunning = false;
            }
            float minutes = Mathf.FloorToInt(time / 60);
            float seconds = Mathf.FloorToInt(time % 60);
            //Debug.Log(minutes + " : " + seconds);
            timer.text = minutes + " : " + seconds;
        }

        if(time == 0)
        {
            //var text = GameObject.FindObjectsOfType<AimController>().score;
            //if (GameObject.FindObjectOfType<AimController>().name.Contains("P2"))
            //{
                scoreP2 = GameObject.FindObjectOfType<AimController>().score;
            //}
            
            if (!over)
            {
                var go = Instantiate(end) as Canvas;
                var text = go.GetComponentsInChildren<Text>();
                foreach (var item in text)
                {
                    if (item.name.Contains("P2")) item.text = "Score P2 : " + scoreP2;
                }
               


                over = true;
            }
            Time.timeScale = 0;
        }
    }

}
