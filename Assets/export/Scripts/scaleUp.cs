﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scaleUp : MonoBehaviour
{
    // Start is called before the first frame update
    public AnimationCurve scaleCurve;
    public GameObject objectToBump;
    Vector3 scale;
    Vector3 baseScale;
    float scaleTime;
    bool isScaling;
    void Start()
    {
        baseScale = objectToBump.transform.localScale;
    }

    // Update is called once per frame
    void Update()
    {
        if (isScaling)
        {
            scale = objectToBump.transform.localScale;
            scaleTime += Time.deltaTime;
            if (scaleTime < GetShakeDuration())
            {
                float shakeRange = scaleCurve.Evaluate(scaleTime);
                objectToBump.transform.localScale = scale * (1 + shakeRange);
            }
            //sinon 
            else
            {
                objectToBump.transform.localScale = baseScale;
                isScaling = false;
            }
        }
    }

    private float GetShakeDuration()
    {
        int nbKeyFrames = scaleCurve.keys.Length;
        if (nbKeyFrames >= 2)
        {
            return scaleCurve.keys[nbKeyFrames - 1].time;
        }
        return 0f;
    }
    private void ResetShake()
    {
        if (!isScaling)
        {
            scale = baseScale;
        }
        scaleTime = 0.0f;
        isScaling = true;
    }

    public void Bumped()
    {
        ResetShake();
    }
}
